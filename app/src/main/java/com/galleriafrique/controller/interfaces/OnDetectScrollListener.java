package com.galleriafrique.controller.interfaces;

/**
 * Created by osifo on 11/10/15.
 */
public interface OnDetectScrollListener {

    void onUpScrolling();

    void onDownScrolling();
}